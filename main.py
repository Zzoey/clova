import argparse
import numpy as np
import os
import tensorflow as tf
from loginfo import log
from tqdm import tqdm

configuration = {
    'hid_d': 40,
    'n_hid_l': 2,
    'log_fr' : 100,
    'summary_fr' : 10,
    'save_fr' : 1000,
    'eval_fr' : 1000,
}

tf_config = tf.ConfigProto(inter_op_parallelism_threads=4,intra_op_parallelism_threads=4)

def MLP_weights(inp_d, out_d):
    weights = {}
    weights['w1'] = tf.Variable(tf.truncated_normal([inp_d, configuration['hid_d']], stddev=0.01))
    weights['b1'] = tf.Variable(tf.zeros([configuration['hid_d']]))
    for i in range(1, configuration['n_hid_l']):
        weights['w'+str(i+1)] = tf.Variable(tf.truncated_normal(
            [configuration['hid_d'], configuration['hid_d']], stddev=0.01))
        weights['b'+str(i+1)] = tf.Variable(tf.zeros([configuration['hid_d']]))
    weights['w'+str(configuration['n_hid_l']+1)] = tf.Variable(
        tf.truncated_normal([configuration['hid_d'], out_d], stddev=0.01))
    weights['b'+str(configuration['n_hid_l']+1)] = tf.Variable(
        tf.zeros([out_d]))
    return weights

def construct_l(inp, activation_fn, reuse, norm, is_train, scope):
    if norm == 'batch_norm':
        out = tf.contrib.layers.batch_norm(inp, activation_fn=activation_fn,
                                           reuse=reuse, is_training=is_train,
                                           scope=scope)
    elif norm == 'None':
        out = activation_fn(inp)
    else:
        ValueError('Can\'t recognize {}'.format(norm))
    return out

def MLP(inp, weights, reuse, norm, is_train, prefix='fc'):
    h = construct_l(tf.matmul(inp, weights['w1']) + weights['b1'],
                             activation_fn=tf.nn.relu, reuse=reuse, is_train=is_train,
                             norm=norm, scope='1.'+prefix)
    for i in range(1, configuration['n_hid_l']):
        w = weights['w'+str(i+1)]
        b = weights['b'+str(i+1)]
        h = construct_l(tf.matmul(h, w)+b, activation_fn=tf.nn.relu,
                                 reuse=reuse, norm=norm, is_train=is_train,
                                 scope=str(i+1)+'.'+prefix)
    w = weights['w'+str(configuration['n_hid_l']+1)]
    b = weights['b'+str(configuration['n_hid_l']+1)]
    out = tf.matmul(h, w) + b
    return out


class MAML(object):
    def __init__(self, dataset, inp_d, out_d,
                 alpha, beta, K, batch_size, is_train, num_updates, norm):
        '''
        alpha:      learning rate to calculate the gradient
        beta:       learning rate used for Adam Optimizer
        K:          Hyperparameter - perform K-shot learning
        '''
        self._sess = tf.Session(config=tf_config)
        self._is_train = is_train
        self._dataset = dataset
        self._alpha = alpha
        self._batch_size = batch_size
        self._K = K
        self._task_name = 'FC'
        self._norm = norm
        self._inp_d = inp_d
        self._out_d = out_d
        self._num_updates = num_updates
        self._meta_optimizer = tf.train.AdamOptimizer(beta)
        self._avoid_second_derivative = False
        self._meta_train_x = tf.placeholder(tf.float32)
        self._meta_train_y = tf.placeholder(tf.float32)
        self._meta_val_x = tf.placeholder(tf.float32)
        self._meta_val_y = tf.placeholder(tf.float32)
        self._loss_fn = tf.losses.mean_squared_error
        self.build_graph(inp_d, out_d, norm=norm)
        self._summary_dir = os.path.join('log', self._task_name)
        self._checkpoint_dir = os.path.join('checkpoint', self._task_name)
        self._saver = tf.train.Saver(max_to_keep=10)
        if self._is_train:
            if not os.path.exists(self._summary_dir):
                os.makedirs(self._summary_dir)
            self._writer = tf.summary.FileWriter(self._summary_dir, self._sess.graph)
            if not os.path.exists(self._checkpoint_dir):
                os.makedirs(self._checkpoint_dir)
        log.infov("Initialize all variables")
        self._sess.run(tf.global_variables_initializer())

    def build_graph(self, inp_d, out_d, norm):
        self._weights = MLP_weights(inp_d, out_d)
        def metastep_graph(inp):
            meta_train_x, meta_train_y, meta_val_x, meta_val_y = inp
            meta_train_loss_list = []
            meta_val_loss_list = []
            weights = self._weights
            meta_train_output = MLP(meta_train_x, weights,reuse=False, norm=norm,
                                                       is_train=self._is_train)
            meta_train_loss = self._loss_fn(meta_train_y, meta_train_output)
            meta_train_loss = tf.reduce_mean(meta_train_loss)
            meta_train_loss_list.append(meta_train_loss)
            grads = dict(zip(weights.keys(),
                         tf.gradients(meta_train_loss, list(weights.values()))))
            new_weights = dict(zip(weights.keys(),
                               [weights[key]-self._alpha*grads[key]
                                for key in weights.keys()]))
            if self._avoid_second_derivative:
                new_weights = tf.stop_gradients(new_weights)
            meta_val_output = MLP(meta_val_x, new_weights,reuse=True, norm=norm,
                                                     is_train=self._is_train)
            meta_val_loss = self._loss_fn(meta_val_y, meta_val_output)
            meta_val_loss = tf.reduce_mean(meta_val_loss)
            meta_val_loss_list.append(meta_val_loss)
            for _ in range(self._num_updates-1):
                meta_train_output = MLP(meta_train_x, new_weights,reuse=True, norm=norm,
                                                           is_train=self._is_train)
                meta_train_loss = self._loss_fn(meta_train_y, meta_train_output)
                meta_train_loss = tf.reduce_mean(meta_train_loss)
                meta_train_loss_list.append(meta_train_loss)
                grads = dict(zip(new_weights.keys(),
                                 tf.gradients(meta_train_loss, list(new_weights.values()))))
                new_weights = dict(zip(new_weights.keys(),
                                       [new_weights[key]-self._alpha*grads[key]
                                        for key in new_weights.keys()]))
                if self._avoid_second_derivative:
                    new_weights = tf.stop_gradients(new_weights)
                meta_val_output = MLP(meta_val_x, new_weights,reuse=True, norm=norm,
                                                         is_train=self._is_train)
                meta_val_loss = self._loss_fn(meta_val_y, meta_val_output)
                meta_val_loss = tf.reduce_mean(meta_val_loss)
                meta_val_loss_list.append(meta_val_loss)

            return [meta_train_loss_list, meta_val_loss_list, meta_train_output, meta_val_output]

        output_dtype = [[tf.float32]*self._num_updates, [tf.float32]*self._num_updates,
                        tf.float32, tf.float32]
        result = tf.map_fn(metastep_graph,
                           elems=(self._meta_train_x, self._meta_train_y,
                                  self._meta_val_x, self._meta_val_y),
                           dtype=output_dtype, parallel_iterations=self._batch_size)
        meta_train_losses, meta_val_losses, meta_train_output, meta_val_output = result
        self._meta_val_output = meta_val_output
        self._meta_train_output = meta_train_output
        meta_train_loss = tf.reduce_mean(meta_train_losses[-1])
        meta_val_loss = tf.reduce_mean(meta_val_losses[-1])
        self._meta_train_loss = meta_train_loss
        self._meta_val_loss = meta_val_loss
        self._meta_train_op = self._meta_optimizer.minimize(meta_val_loss)
        self._meta_train_loss_sum = tf.summary.scalar('loss/meta_train_loss', meta_train_loss)
        self._meta_val_loss_sum = tf.summary.scalar('loss/meta_val_loss', meta_val_loss)
        self._summary_op = tf.summary.merge_all()

    def learn(self, batch_size, dataset, max_steps):
        for step in range(int(max_steps)):
            meta_val_loss, meta_train_loss, summary_str = self.train_step(dataset, batch_size, step)
            if step % configuration['summary_fr'] == 0:
                self._writer.add_summary(summary_str, step)
            if step % configuration['log_fr'] == 0:
                log.info("Step: {}/{}, Meta train loss: {:.4f}, Meta val loss: {:.4f}".format(
                    step, int(max_steps), meta_train_loss, meta_val_loss))
            #if step % configuration['save_fr'] == 0:
                #log.infov("Save checkpoint-{}".format(step))
                #self._saver.save(self._sess, os.path.join(self._checkpoint_dir, 'checkpoint'),
                                 #global_step=step)
            if step % configuration['eval_fr'] == 0:
                self.eval(dataset, 100, False)

    def eval(self, dataset, test_steps, draw):
        accumulated_val_loss = []
        accumulated_train_loss = []
        for step in tqdm(range(test_steps)):
            output, val_loss, train_loss, amplitude, phase, inp = \
                self.test_step(dataset, 1)
            if not self._is_train and draw:
                for am, ph in zip(amplitude, phase):
                    dataset.visualize(am, ph, inp[:, self._K:, :], output,
                                      path=os.path.join(draw_dir, '{}.png'.format(step)))

            accumulated_val_loss.append(val_loss)
            accumulated_train_loss.append(train_loss)
        val_loss_mean = sum(accumulated_val_loss)/test_steps
        train_loss_mean = sum(accumulated_train_loss)/test_steps
        log.infov("[Evaluate] Meta train loss: {:.4f}, Meta val loss: {:.4f}".format(
            train_loss_mean, val_loss_mean))

    def train_step(self, dataset, batch_size, step):
        batch_input, batch_target, _, _ = dataset.get_batch(batch_size, resample=True)
        feed_dict = {self._meta_train_x: batch_input[:, :self._K, :],
                     self._meta_train_y: batch_target[:, :self._K, :],
                     self._meta_val_x: batch_input[:, self._K:, :],
                     self._meta_val_y: batch_target[:, self._K:, :]}
        _, summary_str, meta_val_loss, meta_train_loss = \
            self._sess.run([self._meta_train_op, self._summary_op,
                           self._meta_val_loss, self._meta_train_loss],
                           feed_dict)
        return meta_val_loss, meta_train_loss, summary_str

    def test_step(self, dataset, batch_size):
        batch_input, batch_target, amplitude, phase = dataset.get_batch(batch_size, resample=True)
        feed_dict = {self._meta_train_x: batch_input[:, :self._K, :],
                     self._meta_train_y: batch_target[:, :self._K, :],
                     self._meta_val_x: batch_input[:, self._K:, :],
                     self._meta_val_y: batch_target[:, self._K:, :]}
        meta_val_output, meta_val_loss, meta_train_loss = \
            self._sess.run([self._meta_val_output, self._meta_val_loss,
                           self._meta_train_loss],
                           feed_dict)
        return meta_val_output, meta_val_loss, meta_train_loss, amplitude, phase, batch_input


def main(args):
    if args.dataset == 'sin':
        from sindata import dataset
        dataset = dataset(args.K)
    else:
        ValueError("Invalid dataset")
    model = MAML(dataset,dataset.dim_input,dataset.dim_output,
                 args.alpha,args.beta,args.K,
                 args.batch_size,args.is_train,args.num_updates, args.norm)
    if args.is_train:
        model.learn(args.batch_size, dataset, args.max_steps)
    else:
        model.eval(dataset, args.test_sample, args.draw)

if __name__ == '__main__':
    parser = argparse.ArgumentParser("Tensorflow Implementation of MAML")
    parser.add_argument('--dataset', help='environment ID', choices=['sin'],required=True)
    parser.add_argument('--K', type=int, default=10)
    parser.add_argument('--num_updates', type=int, default=3)
    parser.add_argument('--norm', choices=['None', 'batch_norm'], default='batch_norm')
    parser.add_argument('--is_train', action='store_true', default=False)
    parser.add_argument('--max_steps', type=int, default=7e4)
    parser.add_argument('--alpha', type=float, default=1e-3)
    parser.add_argument('--beta', type=float, default=1e-3)
    parser.add_argument('--batch_size', type=int, default=25)
    parser.add_argument('--test_sample', type=int, default=100)
    parser.add_argument('--draw', action='store_true', default=False)
    args = parser.parse_args()
    main(args)
