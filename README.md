python main.py --dataset sin --K 10 --is_train to run

The task is regression on a sin curve and K is the parameter feeded to perform K shot learning.

main.py contains MAML class and all necessary functions for a generic Implementation
sindata.py contains functions to generate a sincurve

Dependencies:
Tensorflow
matplotlib
python 2.7.*
