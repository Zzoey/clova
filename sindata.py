import numpy as np
import matplotlib.pyplot as plt

configuration = {
    'amplitude_range': [0.5, 7.0],
    'phase_range': [0, np.pi],
    'x_range': [-2.0, 2.0],
    'visualize_sample_point': 70
}


class dataset(object):
    def __init__(self, K_shots=5):
        self._K = K_shots*2
        self.dim_input = 1
        self.dim_output = 1
        self.name = 'sin'
    def _resample_task(self, batch_size, verbose):
        self._sample_amplitude = np.random.uniform(configuration['amplitude_range'][0],
                                                   configuration['amplitude_range'][1], batch_size)
        self._sample_phase = np.random.uniform(configuration['phase_range'][0],
                                               configuration['phase_range'][1], batch_size)
        self._sample_amplitude_tile = np.tile(self._sample_amplitude, [self._K, self.dim_input, 1])
        self._sample_amplitude_tile = np.transpose(self._sample_amplitude_tile, [2, 0, 1])
        self._sample_phase_tile = np.tile(self._sample_phase, [self._K, self.dim_input, 1])
        self._sample_phase_tile = np.transpose(self._sample_phase_tile, [2, 0, 1])
        if verbose:
            print("Mean of the amplitude", np.mean(self._sample_amplitude))
            print("Mean of the phase", np.mean(self._sample_phase))

    def get_batch(self, batch_size, resample=False, verbose=False):
        if resample:
            self._resample_task(batch_size, verbose)
        x = np.random.uniform(configuration['x_range'][0], configuration['x_range'][1],
                              [batch_size, self._K, self.dim_input])
        y = self._sample_amplitude_tile*np.sin(x - self._sample_phase_tile)
        return x, y, self._sample_amplitude, self._sample_phase

    def visualize(self, amplitude, phase, inp, output, path):
        inp = np.squeeze(inp)
        output = np.squeeze(output)
        x = np.linspace(configuration['x_range'][0], configuration['x_range'][1], configuration['visualize_sample_point'])
        curve = amplitude*np.sin(x - phase)
        plt.plot(x, curve, '.')
        plt.plot(inp, output, 'v')
        plt.savefig(path)
        plt.close()
